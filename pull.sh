#!/bin/sh

# some versions of composer complain about HOME not set
export HOME=/set/to/your/home/path

#just a dir, do not add composer in the end
COMPOSER_PATH=/where/is/your/composer/

# identificator for repository
REPO1="your-repo-name"

# this is where the directories will be created
WORK_DIR1=/set/to/root/of/your/web/server

# all releases will sit in this subdirectory of WORK_DIR
RELS_DIR=git_releases

# name of link from WORK_DIR to latest release in RELS_DIR
# this ($WORK_DIR/CURRENT_REL_DIR) should be set as your webservers new root directory
CURRENT_REL_LINK=release_from_git

GIT_REPO_URL=git@bitbucket.org:esuit/webhook-git-pull.git                                                                                                                                                                                              

# if you have stuff in your repo that need to be above the web root dir, set this to your actual web root in repository
# i.e. WEB_DIR_IN_REPO="reponame/htdocs"
WEB_DIR_IN_REPO=""

if [ "$1" == "$REPO1" ]; then                                                                                                                                                     

    cd $WORK_DIR1
                                                                                                                                                                                                                                          
    DIR_NAME=`date +%F-%H%M` 
    # if it does not exist, create. if exists, get an error and proudly ignore it
    mkdir $RELS_DIR                                                                                                                                                                                                                                           
    mkdir $RELS_DIR/$DIR_NAME    
    cd $RELS_DIR/$DIR_NAME   
    
    git clone $GIT_REPO_URL
    if [[ -n "$WEB_DIR_IN_REPO" ]]; then
      cd $WEB_DIR_IN_REPO                                                                                                                                                                                                                          
    fi
    
    $COMPOSER_PATH/composer install 2>&1                                                                                                                                                                                           
    
    cd $WORK_DIR
    rm $CURRENT_REL_LINK
    ln -s $WORK_DIR/$RELS_DIR/$DIR_NAME/$WEB_DIR_IN_REPO $CURRENT_REL_LINK                                                                                                                                                                     
fi   